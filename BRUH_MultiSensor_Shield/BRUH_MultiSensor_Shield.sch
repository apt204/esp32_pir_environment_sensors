EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 5F679819
P 4350 4520
F 0 "J1" H 4268 4195 50  0000 C CNN
F 1 "Conn_01x03" H 4268 4286 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4350 4520 50  0001 C CNN
F 3 "~" H 4350 4520 50  0001 C CNN
	1    4350 4520
	-1   0    0    1   
$EndComp
$Comp
L Device:LED_RGBC D1
U 1 1 5F679EF1
P 4810 3570
F 0 "D1" H 4810 4067 50  0000 C CNN
F 1 "LED_RGBC" H 4810 3976 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm-4_RGB" H 4810 3520 50  0001 C CNN
F 3 "~" H 4810 3520 50  0001 C CNN
	1    4810 3570
	1    0    0    -1  
$EndComp
$Comp
L Sensor:DHT11 U1
U 1 1 5F67AB4F
P 5720 3530
F 0 "U1" H 5476 3576 50  0000 R CNN
F 1 "DHT11" H 5476 3485 50  0000 R CNN
F 2 "Sensor:Aosong_DHT11_5.5x12.0_P2.54mm" H 5720 3130 50  0001 C CNN
F 3 "http://akizukidenshi.com/download/ds/aosong/DHT11.pdf" H 5870 3780 50  0001 C CNN
	1    5720 3530
	1    0    0    -1  
$EndComp
Text Notes 4130 4180 0    50   ~ 0
AM312
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 5F67BC73
P 3420 4610
F 0 "J2" H 3338 4285 50  0000 C CNN
F 1 "Conn_01x03" H 3338 4376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 3420 4610 50  0001 C CNN
F 3 "~" H 3420 4610 50  0001 C CNN
	1    3420 4610
	-1   0    0    1   
$EndComp
Text Notes 3220 4220 0    50   ~ 0
TEMT600
$Comp
L esp32_devkitc4:esp32_devkitc_v4 U2
U 1 1 5F67D0C9
P 6460 2800
F 0 "U2" H 7210 2965 50  0000 C CNN
F 1 "esp32_devkitc_v4" H 7210 2874 50  0000 C CNN
F 2 "old:esp32_devkitC_v4" H 6460 2800 50  0001 C CNN
F 3 "" H 6460 2800 50  0001 C CNN
	1    6460 2800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x19 J5
U 1 1 5F681BAB
P 8680 3930
F 0 "J5" H 8760 3972 50  0000 L CNN
F 1 "Conn_01x19" H 8760 3881 50  0000 L CNN
F 2 "" H 8680 3930 50  0001 C CNN
F 3 "~" H 8680 3930 50  0001 C CNN
	1    8680 3930
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_C_Plug_USB2.0 P1
U 1 1 5FB0BF3B
P 3575 2750
F 0 "P1" H 3682 3617 50  0000 C CNN
F 1 "USB_C_Plug_USB2.0" H 3682 3526 50  0000 C CNN
F 2 "" H 3725 2750 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 3725 2750 50  0001 C CNN
	1    3575 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_C_Receptacle J3
U 1 1 5FB0D8AF
P 1800 2575
F 0 "J3" H 1907 3842 50  0000 C CNN
F 1 "USB_C_Receptacle" H 1907 3751 50  0000 C CNN
F 2 "Connector_USB:USB_C_Receptacle_Amphenol_12401610E4-2A_CircularHoles" H 1950 2575 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 1950 2575 50  0001 C CNN
	1    1800 2575
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_C_Receptacle_USB2.0 J4
U 1 1 5FB0F074
P 5100 1850
F 0 "J4" H 5207 2717 50  0000 C CNN
F 1 "USB_C_Receptacle_USB2.0" H 5207 2626 50  0000 C CNN
F 2 "Connector_USB:USB_C_Receptacle_GCT_USB4085" H 5250 1850 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 5250 1850 50  0001 C CNN
	1    5100 1850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
