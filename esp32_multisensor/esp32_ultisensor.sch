EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_RF-Transceiver-Modules:ESP32-WROOM-32 MOD1
U 1 1 5F631BE3
P 3450 2350
F 0 "MOD1" H 3650 2653 60  0000 C CNN
F 1 "ESP32-WROOM-32" H 3650 2547 60  0000 C CNN
F 2 "digikey-footprints:ESP32-WROOM-32D" H 3650 2550 60  0001 L CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 3650 2650 60  0001 L CNN
F 4 "1904-1010-1-ND" H 3650 2750 60  0001 L CNN "Digi-Key_PN"
F 5 "ESP32-WROOM-32" H 3650 2850 60  0001 L CNN "MPN"
F 6 "RF/IF and RFID" H 3650 2950 60  0001 L CNN "Category"
F 7 "RF Transceiver Modules" H 3650 3050 60  0001 L CNN "Family"
F 8 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 3650 3150 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/espressif-systems/ESP32-WROOM-32/1904-1010-1-ND/8544305" H 3650 3250 60  0001 L CNN "DK_Detail_Page"
F 10 "SMD MODULE, ESP32-D0WDQ6, 32MBIT" H 3650 3350 60  0001 L CNN "Description"
F 11 "Espressif Systems" H 3650 3450 60  0001 L CNN "Manufacturer"
F 12 "Active" H 3650 3550 60  0001 L CNN "Status"
	1    3450 2350
	1    0    0    -1  
$EndComp
$Comp
L dk_Gas-Sensors:CCS811B-JOPD500 U1
U 1 1 5F632AFE
P 6400 3200
F 0 "U1" H 6450 4003 60  0000 C CNN
F 1 "CCS811B-JOPD500" H 6450 3897 60  0000 C CNN
F 2 "digikey-footprints:VOC_Sensor_LGA-10-1EP-2.7x4mm_CCS811B" H 6600 3400 60  0001 L CNN
F 3 "https://ams.com/documents/20143/36005/CCS811_DS000459_7-00.pdf" H 6600 3500 60  0001 L CNN
F 4 "CCS811B-JOPD500CT-ND" H 6600 3600 60  0001 L CNN "Digi-Key_PN"
F 5 "CCS811B-JOPD500" H 6600 3700 60  0001 L CNN "MPN"
F 6 "Sensors, Transducers" H 6600 3800 60  0001 L CNN "Category"
F 7 "Gas Sensors" H 6600 3900 60  0001 L CNN "Family"
F 8 "https://ams.com/documents/20143/36005/CCS811_DS000459_7-00.pdf" H 6600 4000 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/ams/CCS811B-JOPD500/CCS811B-JOPD500CT-ND/6569300" H 6600 4100 60  0001 L CNN "DK_Detail_Page"
F 10 "LOW POWER DIGITAL SENSOR FOR IND" H 6600 4200 60  0001 L CNN "Description"
F 11 "ams" H 6600 4300 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6600 4400 60  0001 L CNN "Status"
	1    6400 3200
	1    0    0    -1  
$EndComp
$Comp
L dk_Specialized-Sensors:BME680 U2
U 1 1 5F633330
P 7850 3750
F 0 "U2" H 8394 3845 60  0000 L CNN
F 1 "BME680" H 8394 3747 50  0000 L CNN
F 2 "digikey-footprints:BME680" H 8050 3950 60  0001 L CNN
F 3 "https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BME680-DS001.pdf" H 8050 4050 60  0001 L CNN
F 4 "828-1077-1-ND" H 8050 4150 60  0001 L CNN "Digi-Key_PN"
F 5 "BME680" H 8050 4250 60  0001 L CNN "MPN"
F 6 "Sensors, Transducers" H 8050 4350 60  0001 L CNN "Category"
F 7 "Specialized Sensors" H 8050 4450 60  0001 L CNN "Family"
F 8 "https://ae-bst.resource.bosch.com/media/_tech/media/datasheets/BST-BME680-DS001.pdf" H 8050 4550 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/bosch-sensortec/BME680/828-1077-1-ND/7401321" H 8050 4650 60  0001 L CNN "DK_Detail_Page"
F 10 "SENSOR RH PRESSURE TEMP VOC" H 8050 4750 60  0001 L CNN "Description"
F 11 "Bosch Sensortec" H 8050 4850 60  0001 L CNN "Manufacturer"
F 12 "Active" H 8050 4950 60  0001 L CNN "Status"
	1    7850 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 5F633F21
P 7825 2550
F 0 "J1" H 7905 2592 50  0000 L CNN
F 1 "Conn_01x03" H 7905 2501 50  0000 L CNN
F 2 "" H 7825 2550 50  0001 C CNN
F 3 "~" H 7825 2550 50  0001 C CNN
	1    7825 2550
	1    0    0    -1  
$EndComp
Text Notes 8025 2400 0    50   ~ 0
HC-SR501
Text Notes 5820 4370 0    50   ~ 0
EKMB1305112K
$Comp
L Device:LED_BGRA D?
U 1 1 5F65B700
P 7190 4830
F 0 "D?" H 7190 5327 50  0000 C CNN
F 1 "LED_BGRA" H 7190 5236 50  0000 C CNN
F 2 "" H 7190 4780 50  0001 C CNN
F 3 "~" H 7190 4780 50  0001 C CNN
	1    7190 4830
	1    0    0    -1  
$EndComp
$EndSCHEMATC
